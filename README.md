# CI/CD Demo DWI16
Dies ist das während der Vorlesung "Softwareengineering" am 21.05.19 erarbeitete Repository.
Ziel war es, einen Einblick in DevOps, insbesondere in die Automatisierung des Entwicklungsworkflows mithilfe von CI/CD am Beispiel der GitLab CI zu geben.

- Das Deployment zum Staging-System mit GitLab Pages ist [hier](https://moert09.gitlab.io/dwi16-demo/) zu finden.
- Das Deployment zum Produktivsystem mit Firebase ist [hier](https://dwi16-3667c.web.app/) zu finden.

Nachfolgend die Standard Readme-Datei.

# Dwi16

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
